# RCBot2 for Windows and Linux (TF2, HL2:DM, DOD:S)

[![pipeline status](https://gitlab.com/dargh/rcbot2/badges/master/pipeline.svg)](https://gitlab.com/dargh/rcbot2/commits/master)


## Information
This is a fork of [APG]RoboCop's fork of Cheeseh's RCBot2 repository.

This is my personal rcbot repo that adds some changes that I want or my friends want.\
I've made this public for anyone who might like the simple features I've added.

[Installation](http://rcbot.bots-united.com/forums/index.php?showtopic=1967)

## Console Commands
|     convar      |  Value Type | Description |
| :-------------: | :---------: | :---------: |
| rcbot_smart_check | boolean | When enabled if a bot sees a spy in tf2 it will check if the spy is disguised as a class that exists on their team.

## Building
### Windows
Make sure you have Visual Studio 2013 installed.
Other than that it should compile fine, even with the latest Visual Studio 2017.

### Linux

**Note: You need gcc-4.8 and g++-4.8!**

* Go to `linux_sdk`
* `make -f Makefile.rcbot2 vcpm`
* `make -f Makefile.rcbot2 genmf` (this will generate the Makefiles)
* `make -f Makefile.rcbot2 all -j4`
And a `RCBot2Meta_i486.so` should appear in the folder.
---

## Download
[Windows](https://gitlab.com/dargh/rcbot2/pipelines/)

Be sure to check out the rcbot2 forums for help. [rcbot.bots-united.com forum](http://rcbot.bots-united.com/forums/)\
RoboCop also setup a discord: [bots-united.com discord](https://discord.gg/BbxR5wY)